<?php

namespace Drupal\commerce_nz\Plugin\Commerce\TaxNumberType;

use Drupal\commerce_tax\Plugin\Commerce\TaxNumberType\TaxNumberTypeBase;

/**
 * Provides the New Zealand GST number type.
 *
 * @CommerceTaxNumberType(
 *   id = "new_zealand_gst",
 *   label = "New Zealand GST",
 *   countries = {
 *     "NZ",
 *   },
 *   examples = {"096-259-824"}
 * )
 */
class NewZealandGst extends TaxNumberTypeBase {

  /**
   * {@inheritdoc}
   */
  public function validate($tax_number) {
    return $this->checkIrd((int) $this->canonicalize($tax_number));
  }

  /**
   * Check that NZ IRD # meets the IRD pattern spec.
   *
   * @param int $ird
   *   GST number to check.
   *
   * @return bool
   *   TRUE if the number meets the spec, FALSE if not.
   */
  private function checkIrd(int $ird): bool {
    // Should be 8 characters in length, it arrives as an int.
    if ($ird < 10000000 || $ird > 150000000) {
      return FALSE;
    }
    $ird_string = (string) $ird;

    for ($n = 0; $n < strlen($ird_string); $n++) {
      $ird_array[] = $ird_string[$n];
    }

    array_pop($ird_array);
    if (count($ird_array) < 8) {
      array_unshift($ird_array, '0');
    }
    $primary_weights = [3, 2, 7, 6, 5, 4, 3, 2];
    $secondary_weights = [7, 4, 3, 2, 5, 2, 7, 6];

    $primary_sum = $this->calculateSum($ird_array, $primary_weights);

    $rem = $this->calculateCheckDigit($primary_sum);
    if ($rem == substr($ird_string, strlen($ird_string)-1, 1)) {
      return TRUE;
    }
    if ($rem === 10) {
      $secondary_sum = $this->calculateSum($ird_array, $secondary_weights);
      $rem = $this->calculateCheckDigit($secondary_sum);
      if ($rem == substr($ird_string, strlen($ird_string)-1, 1)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  private function calculateCheckDigit($sum) {
    $rem = $sum % 11;
    if ($rem !== 0) {
      $rem = 11 - $rem;
    }
    return $rem;
  }


  private function calculateSum(array $ird, array $weights) {
    $ird_length  = count($ird);
    $start = count($weights) - $ird_length;
    $sum = 0;
    for ($i = $start; $i < $ird_length; $i++) {
      $sum += $ird[$i] * $weights[$i];
    }
    return $sum;
  }


}
